import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import {
  Image,
  Product,
  Price,
  productResponse,
  VariantInfo,
} from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private _productList: Product[] = [];

  constructor(private readonly http: HttpClient) {}

  // Fetches products from API and maps them to a list of products
  public fetchProducts(): void {
    const body = '';
    this.http
      .post<productResponse>(
        '/ecommerce/products?RepositoryName=products&QueryName=products',
        body
      )
      .pipe(
        map((response: productResponse) => {
          return response.Products;
        })
      )
      .subscribe({
        next: (products: Product[]) => {
          this._productList = products;
        },
      });
  }

  public getProductList(): Product[] {
    return this._productList;
  }

  // Gets a product from the product list by ID
  public getProduct(productId: string): Product {
    // Create empty product to avoid potential return error
    let price: Price = { Price: 0 };
    let image: Image = { Value: '' };
    let variantInfo: VariantInfo = { Image: image };
    let product: Product = {
      Id: '',
      Title: '',
      Name: '',
      Price: price,
      VariantInfo: variantInfo,
    };
    for (const product of this._productList) {
      if (product.Id == productId) {
        return product;
      }
    }
    return product;
  }
}
