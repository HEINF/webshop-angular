import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart, OrderLines, Price } from '../models/cart.model';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  // Initializes an empty cart
  private _price: Price = { Price: 0 };
  private _orderLines: OrderLines = {
    Id: '',
    ProductName: '',
    ProductId: '',
    Quantity: 0,
    Price: this._price,
  };
  private _cart: Cart = {
    Id: '',
    Secret: '',
    Price: this._price,
    OrderLines: [this._orderLines],
  };

  constructor(private readonly http: HttpClient) {}

  // Fetches a cart if a secret is stored. If not creates a new cart.
  public fetchCart(): void {
    if (this._cart.Secret !== '') {
      this.http
        .get<Cart>(`/ecommerce/carts/${this._cart.Secret}`)
        .subscribe((cart: Cart) => {
          this._cart = cart;
        });
    } else {
      this.createCart();
    }
  }

  // Creates a new cart
  public createCart(): void {
    const body = '';
    this.http
      .post<Cart>(
        '/ecommerce/carts?currencyCode=USD&countryCode=DK&languageId=LANG1',
        body
      )
      .subscribe((cart: Cart) => {
        this._cart = cart;
      });
    console.log(this._cart);
  }
  // Adds an item to cart. If item already exists in the cart item quantity is incremented instead.
  public addToCart(productId: string): void {
    let orderHasProduct = false;
    for (const orderLine of this._cart.OrderLines) {
      if (orderLine.ProductId == productId) {
        orderHasProduct = true;
      }
    }
    if (!orderHasProduct) {
      const body = new HttpParams()
        .set('ProductId', productId)
        .set('Quantity', 1);
      this.http
        .post<any>(`/ecommerce/carts/${this._cart.Secret}/items`, body)
        .subscribe((data: any) => {
          this.fetchCart();
        });
    } else {
      this.incrementItemQuantity(productId);
    }
  }

  // Adjust quantity for ALL orderlines containing the GIVEN ID (Ie. this method assumes that a product can only appear in one orderline for a given order)
  public updateItemQuantity(quantity: number, productId: string): void {
    for (const orderLine of this._cart.OrderLines) {
      if (orderLine.ProductId == productId) {
        const body = new HttpParams().set('Quantity', quantity);
        this.http
          .patch<any>(
            `/ecommerce/carts/${this._cart.Secret}/items/${orderLine.Id}`,
            body
          )
          .subscribe((data: any) => {
            this.fetchCart();
          });
      }
    }
  }

  // Increments quantity for an existing orderline. CAUTION: this method assumes that a product can only appear in one orderline for a given order)
  public incrementItemQuantity(productId: string): void {
    for (const orderLine of this._cart.OrderLines) {
      if (orderLine.ProductId == productId) {
        const quantity = orderLine.Quantity + 1;
        const body = new HttpParams().set('Quantity', quantity);
        this.http
          .patch<any>(
            `/ecommerce/carts/${this._cart.Secret}/items/${orderLine.Id}`,
            body
          )
          .subscribe((data: any) => {
            this.fetchCart();
          });
      }
    }
  }

  // Deletes orderLine based on productId contained in orderline. CAUTION: This assumes that a given product can only appear in one orderline for a given order
  public deleteItem(productId: string): void {
    for (const orderLine of this._cart.OrderLines) {
      if (orderLine.ProductId == productId) {
        this.http
          .delete<any>(
            `/ecommerce/carts/${this._cart.Secret}/items/${orderLine.Id}`
          )
          .subscribe((data: any) => {
            this.fetchCart();
          });
      }
    }
  }

  public getCart(): Cart {
    return this._cart;
  }

  public checkForItem(productId: string): boolean {
    for (const orderLine of this._cart.OrderLines) {
      if (orderLine.ProductId == productId) {
        return true;
      }
    }
    return false;
  }

  public setCartSecret(secret: string): void {
    this._cart.Secret = secret;
  }
}
