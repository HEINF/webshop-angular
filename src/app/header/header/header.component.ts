import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  toShop(): void {
    this.router.navigateByUrl('/shop')
  }

  toCart(): void {
    this.router.navigateByUrl('/cart')
  }

}
