import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CartComponent } from "./cart/cart/cart.component";
import { ProductListComponent } from "./products/product-list/product-list.component";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/shop'
    },
    {
        path: 'cart',
        component: CartComponent

    },
    {
        path: 'shop',
        component: ProductListComponent
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}