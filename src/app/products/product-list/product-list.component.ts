import { Component, OnInit } from '@angular/core';
import { retry } from 'rxjs';
import { Cart } from 'src/app/models/cart.model';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  constructor(
    private readonly productService: ProductService,
    private readonly cartService: CartService
  ) {}

  // Fetch products and cart from API. If no cart, new will be created.
  ngOnInit(): void {
    this.productService.fetchProducts();
    this.cartService.fetchCart();
  }

  addToCart(productId: string): void {
    this.cartService.addToCart(productId);
  }

  // Not currently used
  changeQuantity(quantity: number, productId: string): void {
    this.cartService.updateItemQuantity(quantity, productId);
  }

  // Deltes ALL items of a product regardless of Quantity
  deleteItem(productId: string): void {
    console.log('Delete item clicked');
    this.cartService.deleteItem(productId);
  }
  // Checks if product is in cart
  checkForItem(productId: string): boolean {
    return this.cartService.checkForItem(productId);
  }

  get productList(): Product[] {
    return this.productService.getProductList();
  }

  get cart(): Cart {
    return this.cartService.getCart();
  }
}
