export interface Product {
    Id: string;
    Title: string;
    Name: string;
    Price: Price;
    VariantInfo: VariantInfo;
}

export interface Price {
    Price: number;
}

export interface VariantInfo {
    Image: Image;
}

export interface Image {
    Value: string;
}

export interface Assets {
    Value: string;
}


export interface productResponse {
    Products: Product[];
}
