

export interface Cart{
    Id: string;
    Secret?: string;
    Price: Price;
    OrderLines: OrderLines[];
}

export interface Price{
    Price: number;
}

export interface OrderLines{
    Id: string;
    ProductName: string;
    ProductId: string;
    Quantity: number;
    Price: Price;

}