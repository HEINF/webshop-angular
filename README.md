# DynamicWeb Webshop 
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Prerequisites
- Angular 13
- Node 16
- npm 8

## Install
- Clone repository and open in VS Code.
- Type 'npm install' in terminal.

## Usage
- Type 'ng serve --proxy-config proxy.conf.json' to run
- Open in compatible browser of choice.

## Contributing

Not open for contributions.

## License

Unknown
